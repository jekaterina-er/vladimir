var submitBtn = document.getElementById("submit-btn");
submitBtn.addEventListener("click", function(){
	var firstName = document.getElementById("first-name").value;
	var lastName = document.getElementById("last-name").value;
	var email = document.getElementById("email").value;
	var telNumber = document.getElementById("tel-number").value;
	var city = document.getElementById("city").value;
	var comment = document.getElementById("comment").value
	var form = document.getElementById ("contact-form").value;
	if (firstName == "") {
		var firstName = document.getElementById("first-name");
		firstName.classList.add("empty-look");
	}
	else{
		var firstName = document.getElementById("first-name");
		firstName.classList.remove("empty-look");
	};
	if (lastName == "") {
		var lastName = document.getElementById("last-name");
		lastName.classList.add("empty-look");
	}
	else{
		var lastName = document.getElementById("last-name");
		lastName.classList.remove("empty-look");
	};
	if (email == "") {
		var email = document.getElementById("email");
		email.classList.add("empty-look");
	}
	else{
		var email = document.getElementById("email");
		email.classList.remove("empty-look");
	};
	if (telNumber == "") {
		var telNumber = document.getElementById("tel-number");
		telNumber.classList.add("empty-look");
	}
	else{
		var telNumber = document.getElementById("tel-number");
		telNumber.classList.remove("empty-look");
	};
	if (city == "") {
		var city = document.getElementById("city");
		city.classList.add("empty-look");
	}
	else{
		var city = document.getElementById("city");
		city.classList.remove("empty-look");
	};
	if (comment == "") {
		var comment = document.getElementById("comment");
		comment.classList.add("empty-look");
	}
	else{
		var comment = document.getElementById("comment");
		comment.classList.remove("empty-look");
	};
	if (firstName.value && lastName.value && email.value  && telNumber.value && city.value && comment.value) {
		$('#staticBackdrop').modal('show')
		var form = document.getElementById ("contact-form");
		form.reset();
	}
});