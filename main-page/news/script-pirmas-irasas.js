var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Turėjo elgesio problemų', 'Neturėjo elgesio problemų', 'Atsisakė dalyvauti', 'Nepateikė nuomonės'],
        datasets: [{
            label: 'My First dataset',
            backgroundColor: [
            '#ff6384',
            '#36a2eb',
            '#cc65fe',
            '#ffce56'
            ],
            borderColor: 'rgb(255, 99, 132)',
            data: [7, 4, 4, 2]
        }]
    },
    options: {}
});